;(function ($) {
    $(document).ready(function () {
        $('[name="job_change_state"]').change(function () {
            $element = $(this);
            $.ajax({
                type: 'POST',
                url: '/job/' + $element.data('jobId') + '/changeState',
                data: {
                    newState: $element.val(),
                    role: $element.data('role')
                },
                success: function (answer) {
                    window.location.href = '/job/' + $element.data('jobId');
                },
                error: function (error) {
                    alert('Error');
                },
                dataType: 'json'
            });
        });
    });
}($));