CREATE TABLE IF NOT EXISTS `jobs`
(
    `id`      int(10) unsigned NOT NULL AUTO_INCREMENT,
    `name`    varchar(50)      NOT NULL,
    `state`   varchar(50)      NOT NULL,
    `created` TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `updated` TIMESTAMP        NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    KEY `name` (`name`),
    KEY `state` (`state`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;