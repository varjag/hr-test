<?
$pageTitle = 'Job #' . $job->id;
?>

<? include __DIR__ . '/../base/header.php' ?>

    <div class="container">

        <? if (isset($_SESSION['flash']['message'])) { ?>
            <p class="alert alert-primary">
                <? echo $_SESSION['flash']['message'] ?>
            </p>
        <? } ?>

        <div class="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
            <div class="col p-4 d-flex flex-column position-static">
                <strong class="d-inline-block mb-2 text-primary">
                    <?= htmlspecialchars($job->name) ?>
                </strong>
                <h3 class="mb-3">
                    <? echo $pageTitle ?>
                </h3>
                <p class="card-text mb-auto"><? echo htmlspecialchars($job->state) ?></p>
                <? if ($job->id) { ?>
                    <a href="/job/<? echo $job->id ?>/edit">Edit</a>
                <? } ?>
            </div>
        </div>
    </div>

<? include __DIR__ . '/../base/footer.php' ?>