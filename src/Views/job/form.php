<?
$pageTitle = 'Job';
?>

<? include __DIR__ . '/../base/header.php' ?>

    <div class="container">
        <form class="form-job-create text-center" method="post"
              action="<? echo $job->id ? "/job/$job->id" : '/job/create' ?>">
            <h1 class="h3 mb-3 font-weight-normal">
                <? echo $job->id ? 'Edit job #' . $job->id : 'Create job' ?>
            </h1>

            <div class="form-group">
                <label for="email" class="sr-only">Email</label>
                <input
                    <? echo $job->id ? 'readonly' : '' ?>
                        type="text"
                        id="name"
                        name="name"
                        class="form-control<? echo isset($errors['name']) ? (' is-invalid') : '' ?>"
                        placeholder="name"
                        required
                        autofocus
                        value="<? echo $job->name ?>"
                >
                <? if (isset($errors['name'])) { ?>
                    <div class="invalid-feedback">
                        <? echo $errors['name'] ?>
                    </div>
                <? } ?>
            </div>

            <button class="btn btn-lg btn-primary btn-block" type="submit">
                <? echo $job->id ? 'Update' : 'Create' ?>
            </button>
        </form>
    </div>

<? include __DIR__ . '/../base/footer.php' ?>