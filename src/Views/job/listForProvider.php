<?
/**
 * @var Hr\App\Models\Job $job
 */
$pageTitle = 'Jobs';
?>

<? include __DIR__ . '/../base/header.php' ?>
    <script src="/js/jobList.js"></script>
    <div class="container">
        <div class="my-3 p-3 bg-white rounded box-shadow">
            <h6 class="border-bottom border-gray pb-2 mb-0 clearfix row align-items-center">
                <div class="col-4">
                    Jobs List For Provider
                </div>
                <div class="col-4">
                    <form action="/listForProvider" method="get" title="Sort:">
                        <input type="hidden" name="page" value="1">
                        <div class="row justify-content-center align-items-center">
                            <div class="col-auto">
                                <select class="form-control form-control-sm" name="orderBy">
                                    <option value="">Default</option>
                                    <? foreach ($sortOptions as $key => $value) { ?>
                                        <option value="<? echo $key ?>" <? echo $orderBy === $key ? 'selected' : '' ?>>
                                            <? echo $value ?>
                                        </option>
                                    <? } ?>
                                </select>
                            </div>
                            <div class="col-auto">
                                <button class="btn btn-sm btn-secondary" type="submit">Sort</button>
                            </div>
                        </div>
                    </form>
                </div>
            </h6>

            <? foreach ($jobs as $job) { ?>
                <div class="media text-muted pt-3">
                    <div class=" media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <div class="clearfix">
                            <a href="/job/<? echo $job->id ?>" class="text-gray-dark float-left">
                                <? echo htmlspecialchars($job->name) ?>
                            </a>
                        </div>
                        <? echo 'State: ' . htmlspecialchars($job->state); ?>
                        <div class="row justify-content-center align-items-center">
                            <div class="col-auto">
                                <? $statesToMoveOn = $job->getStatesToMoveOn($job->state, 'provider'); ?>
                                <select
                                        class="form-control form-control-sm"
                                        name="job_change_state"
                                        data-job-id="<? echo $job->id ?>"
                                        data-role="provider"
                                    <? echo $statesToMoveOn ? '' : 'disabled="disabled"' ?>
                                >
                                    <option value="">Select state to move on</option>
                                    <? foreach ($statesToMoveOn as $key => $value) { ?>
                                        <option value="<? echo $key ?>"><? echo "Go to state '$key' by action '{$value['action']}'" ?></option>
                                    <? } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            <? } ?>

            <? if (empty($jobs)) { ?>
                <div class="alert alert-info mt-3">
                    No jobs.
                </div>
            <? } ?>

            <nav class="mt-4">
                <ul class="pagination pagination-sm justify-content-center">
                    <?
                    $currentCount = 0;
                    while ($currentCount < $totalPages) {
                        $currentCount++;
                        ?>
                        <? if ($currentCount === (int)$page) { ?>
                            <li class="page-item active" aria-current="page">
                                <span class="page-link">
                                    <? echo $currentCount ?>
                                    <span class="sr-only">(current)</span>
                                </span>
                            </li>
                        <? } else { ?>
                            <li class="page-item">
                                <a class="page-link"
                                   href="/listForProvider?page=<? echo $currentCount ?>&orderBy=<? echo $orderBy ?>">
                                    <? echo $currentCount ?>
                                </a>
                            </li>
                        <? } ?>
                    <? } ?>
                </ul>
            </nav>
        </div>
    </div>
<? include __DIR__ . '/../base/footer.php' ?>