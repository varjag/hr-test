<?php

namespace Hr\App\Models;

use Hr\App\Persistable;

class Job
{
    use Persistable;

    const PER_PAGE = 2;
    const AVAILABLE_SORT = [];
    const STATE_DRAFT = 'draft';
    const STATE_MACHINE = [
        'draft' => [
            'posted' => [
                'action' => 'post',
                'role' => 'client',
            ],
            'canceled' => [
                'action' => 'cancel',
                'role' => 'client',
            ],
        ],
        'posted' => [
            'draft' => [
                'action' => 'retract',
                'role' => 'client',
            ],
            'pending proposal' => [
                'action' => 'accept request',
                'role' => 'provider',
            ],
        ],
        'pending proposal' => [
            'proposed' => [
                'action' => 'propose',
                'role' => 'provider',
            ],
        ],
        'proposed' => [
            'posted' => [
                'action' => 'reject',
                'role' => 'client',
            ],
            'contracted' => [
                'action' => 'accept',
                'role' => 'client',
            ],
        ],
        'contracted' => [
            /** No states to move on */
        ],
        'canceled' => [
            /** No states to move on */
        ],
    ];

    /**
     * @var \PDO
     */
    private $db;

    public function __construct(\PDO $db)
    {
        $this->db = $db;
    }

    public function create(): Job
    {
        $statement = $this->db->prepare('
            INSERT INTO jobs (name, state) VALUES (:name, :state)
        ');
        $statement->bindValue(':name', $this->name, \PDO::PARAM_STR);
        $statement->bindValue(':state', $this->state, \PDO::PARAM_STR);
        $statement->execute();
        $this->id = $this->db->lastInsertId();
        if (!$this->id) {
            throw new \Exception('Wrong id. Error: '. $this->db->errorCode());
        }
        return $this;
    }

    public function update(): Job
    {
        $statement = $this->db->prepare('
            UPDATE jobs SET name=:name, state=:state WHERE id=:id
        ');
        $statement->bindValue(':id', $this->id, \PDO::PARAM_INT);
        $statement->bindValue(':name', $this->name, \PDO::PARAM_STR);
        $statement->bindValue(':state', $this->state, \PDO::PARAM_STR);
        $statement->execute();
        return $this;
    }

    public function getList($orderBy = 'id desc', $offset = 0, $limit = self::PER_PAGE): array
    {
        $statement = $this->db->prepare("SELECT * FROM jobs ORDER BY ${orderBy} LIMIT :limit OFFSET :offset");
        $statement->bindValue(':limit', $limit, \PDO::PARAM_INT);
        $statement->bindValue(':offset', $offset, \PDO::PARAM_INT);
        $statement->execute();
        $collection = [];
        foreach ($statement->fetchAll(\PDO::FETCH_ASSOC) as $item) {
            $collection[] = (new static($this->db))->fromArray($item);
        }
        return $collection;
    }

    public function count(): int
    {
        $statement = $this->db->prepare('SELECT count(*) FROM jobs');
        $statement->execute();
        return $statement->fetchColumn();
    }

    public function getByIdOrExcept(int $id): Job
    {
        $statement = $this->db->prepare('SELECT * FROM jobs WHERE id = :id');
        $statement->execute([
            'id' => $id,
        ]);
        $entity = (new static($this->db))->fromArray($statement->fetch());
        if (!$entity->id) {
            throw new \Exception('Cant find entity by id');
        }
        return $entity;
    }

    public function getStatesToMoveOn(string $state, string $role): array
    {
        if (!array_key_exists($state, Job::STATE_MACHINE)) {
            throw new \Exception('Cant find state in state machine');
        }
        $statesToMoveOn = [];
        foreach (Job::STATE_MACHINE[$state] as $key => $state) {
            if ($state['role'] === $role) {
                $statesToMoveOn[$key] = $state;
            }
        }
        return $statesToMoveOn;
    }
}
