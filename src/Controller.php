<?php


namespace Hr\App;

use Zend\Diactoros\Response;

abstract class Controller implements ControllerInterface
{
    public function view($path, $params = [])
    {
        return new TemplateResponse($path, $params);
    }
}