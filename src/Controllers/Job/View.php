<?php

namespace Hr\App\Controllers\Job;

use Hr\App\Controller;
use Hr\App\Models\Job;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class View extends Controller
{
    /**
     * @var Job
     */
    private $job;

    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    public function __invoke(ServerRequestInterface $request, array $args): ResponseInterface
    {
        $params = [];
        if (isset($args['id']) && $args['id']) {
            $params['job'] = $this->job->getByIdOrExcept($args['id']);
        }
        return $this->view('job/view', $params);
    }
}
