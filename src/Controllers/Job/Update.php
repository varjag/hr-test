<?php

namespace Hr\App\Controllers\Job;

use Hr\App\Controller;
use Hr\App\Models\Job;
use Hr\App\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class Update extends Controller
{
    /**
     * @var Job
     */
    private $job;
    /**
     * @var Session
     */
    private $session;

    public function __construct(Job $job, Session $session)
    {
        $this->job = $job;
        $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request, array $args): ResponseInterface
    {
        $params = $request->getParsedBody();
        $errors = [];
        $this->job = $this->job->getByIdOrExcept($args['id']);
        if (!($params['name'] ?? null)) {
            $errors['name'] = 'The name field is required';
        }
        if (count($errors)) {
            return $this->view('job/form', [
                'errors' => $errors,
                'job' => $this->job,
            ]);
        }
        $this->job->name = $params['name'];
        $this->job->update();
        $this->session->flash('message', "Job #{$this->job->id} updated");
        return new RedirectResponse('/job/' . $this->job->id);
    }
}
