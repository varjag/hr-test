<?php


namespace Hr\App\Controllers\Job;


use Hr\App\Controller;
use Hr\App\Models\Job;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class ListForClient extends Controller
{
    /**
     * @var Job
     */
    private $job;

    public function __construct(Job $job)
    {
        $this->job = $job;
    }

    public function __invoke(ServerRequestInterface $request, array $args = []): ResponseInterface
    {
        $query_params = $request->getQueryParams();
        $orderBy     = $query_params['orderBy'] ?? '';
        $page         = $query_params['page'] ?? 1;
        if (!is_numeric($page) || $page < 1) {
            $page = 1;
        }
        if (!$orderBy || ($orderBy && !array_key_exists($orderBy, Job::AVAILABLE_SORT))) {
            $orderBy = 'id desc';
        }
        $jobs = $this->job->getList($orderBy, ($page - 1) * Job::PER_PAGE);
        $count = $this->job->count();
        return $this->view('job/listForClient', [
            'jobs'        => $jobs,
            'orderBy'     => $orderBy,
            'page'         => $page,
            'count'        => $count,
            'totalPages'  => (int)ceil($count / Job::PER_PAGE),
            'sort_options' => Job::AVAILABLE_SORT,
        ]);
    }

}