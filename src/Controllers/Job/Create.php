<?php

namespace Hr\App\Controllers\Job;

use Hr\App\Controller;
use Hr\App\Models\Job;
use Hr\App\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;

class Create extends Controller
{
    /**
     * @var Job
     */
    private $job;
    /**
     * @var Session
     */
    private $session;

    public function __construct(Job $job, Session $session)
    {
        $this->job = $job;
        $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request, array $args = []): ResponseInterface
    {
        $params = $request->getParsedBody();
        $name = (string)($params['name'] ?? '');
        $errors = [];
        if (!$name) {
            $errors['name'] = 'The name field is required';
        }
        $this->job->name = $name;
        $this->job->state = Job::STATE_DRAFT;
        if (count($errors)) {
            return $this->view('job/form', [
                'job' => $this->job,
                'errors' => $errors,
            ]);
        }
        $this->job->create();
        $this->session->flash('message', "Job #{$this->job->id} created");
        return new RedirectResponse('/job/' . $this->job->id);
    }
}
