<?php

namespace Hr\App\Controllers\Job;

use Hr\App\Controller;
use Hr\App\Models\Job;
use Hr\App\Session;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\Response\JsonResponse;

class ChangeState extends Controller
{
    /**
     * @var Job
     */
    private $job;
    /**
     * @var Session
     */
    private $session;

    public function __construct(Job $job, Session $session)
    {
        $this->job = $job;
        $this->session = $session;
    }

    public function __invoke(ServerRequestInterface $request, array $args): ResponseInterface
    {
        $params = $request->getParsedBody();
        $errors = [];
        $this->job = $this->job->getByIdOrExcept($args['id']);
        if (!($params['newState'] ?? null)) {
            $errors['newState'] = 'The newState field is required';
        }
        if (!($params['role'] ?? null)) {
            /** TODO get role from session */
            $errors['role'] = 'The role field is required';
        }
        if (count($errors)) {
            return $this->view('job/form', [
                'errors' => $errors,
                'job' => $this->job,
            ]);
        }
        $statesToMoveOn = $this->job->getStatesToMoveOn($this->job->state, $params['role']);
        if (!array_key_exists($params['newState'], $statesToMoveOn)) {
            throw new \Exception("Cant move job to new state: {$params['newState']}");
        }
        $this->job->state = $params['newState'];
        $this->job->update();
        $message = "Job #{$this->job->id} moved to new state: {$params['newState']}";
        $this->session->flash('message', $message);
        return new JsonResponse(['message' => $message], 200);
    }
}
