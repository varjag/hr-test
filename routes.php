<?php

use Hr\App\Controllers;
use League\Route\Router;

return function (Router $router, Psr\Container\ContainerInterface $container) {
    $router->get('/', Controllers\Job\ListForClient::class);
    $router->get('/listForClient', Controllers\Job\ListForClient::class);
    $router->get('/listForProvider', Controllers\Job\ListForProvider::class);
    $router->get('/job/create', Controllers\Job\Form::class);
    $router->post('/job/create', Controllers\Job\Create::class);
    $router->get('/job/{id:number}', Controllers\Job\View::class);
    $router->get('/job/{id:number}/edit', Controllers\Job\Form::class);
    $router->post('/job/{id:number}', Controllers\Job\Update::class);
    $router->post('/job/{id:number}/changeState', Controllers\Job\ChangeState::class);
};